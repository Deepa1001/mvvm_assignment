package com.infosys.telstra_assignment.data.remote

import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Test
import java.util.concurrent.TimeUnit

class RemoteDataSourceTest {
    private lateinit var service: RemoteDataSource
    private val mockServer = MockWebServer()

    private companion object {
        const val SUCCESSFUL_RESPONSE_EMPTY = """{
            "title":"About Canada",
            "rows":[
            {
                "title":"Beavers",
                "description":"Beavers are second only to humans in their ability to manipulate and change their environment. They can measure up to 1.3 metres long. A group of beavers is called a colony",
                "imageHref":"http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/American_Beaver.jpg/220px-American_Beaver.jpg"
            }
            ]
            }"""
    }

    @Test
    fun testSuccessfulResponse() {
        mockServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse()
                    .setResponseCode(200)
                    .setBody(SUCCESSFUL_RESPONSE_EMPTY)
            }
        }
    }

    @Test
    fun testFailedResponse() {
        mockServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse().throttleBody(1024, 5, TimeUnit.SECONDS)
            }
        }
    }
}