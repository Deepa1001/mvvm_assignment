package com.infosys.telstra_assignment.data.remote

import com.infosys.telstra_assignment.data.entities.DataModel
import retrofit2.Response
import retrofit2.http.GET

interface AppService {
    @GET("s/2iodh4vg0eortkl/facts.json")
    suspend fun getAllData(): Response<DataModel>

}