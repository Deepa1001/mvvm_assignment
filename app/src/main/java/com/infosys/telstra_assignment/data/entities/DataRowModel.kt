package com.infosys.telstra_assignment.data.entities

data class DataRowModel(
    var title: String,
    var description: String,
    var imageHref: String
)
