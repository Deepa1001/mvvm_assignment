package com.infosys.telstra_assignment.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

@Entity(tableName = "data_table")
data class DataModel(
    @PrimaryKey(autoGenerate = true) var _id: Int = -1,
    val title: String,
    @TypeConverters(Converters::class)
    val rows: List<DataRowModel>
)

class Converters {
    private val gson = Gson()

    @TypeConverter
    fun stringToList(data: String?): List<DataRowModel> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<DataRowModel>>() {
        }.type
        return gson.fromJson<List<DataRowModel>>(data, listType)
    }

    @TypeConverter
    fun listToString(objects: List<DataRowModel>): String {
        return gson.toJson(objects)
    }
}