package com.infosys.telstra_assignment.data.repository

import com.infosys.telstra_assignment.data.local.DataDao
import com.infosys.telstra_assignment.data.remote.RemoteDataSource
import com.infosys.telstra_assignment.utils.performGetOperation
import javax.inject.Inject

class DataRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: DataDao
) {
    fun getData() = performGetOperation(
        databaseQuery = { localDataSource.getAllData() },
        networkCall = { remoteDataSource.getData() },
        saveCallResult = { localDataSource.insertAll(it) }
    )
}