package com.infosys.telstra_assignment.data.remote

import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val service: AppService
) : BaseDataSource() {

    suspend fun getData() = getResult { service.getAllData() }
}