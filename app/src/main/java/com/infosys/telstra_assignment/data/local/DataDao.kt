package com.infosys.telstra_assignment.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.infosys.telstra_assignment.data.entities.DataModel

@Dao
interface DataDao {

    @Query("SELECT * FROM data_table")
    fun getAllData(): LiveData<DataModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(data_table: DataModel)

}