package com.infosys.telstra_assignment.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.infosys.telstra_assignment.data.local.AppDatabase
import com.infosys.telstra_assignment.data.local.DataDao
import com.infosys.telstra_assignment.data.remote.AppService
import com.infosys.telstra_assignment.data.remote.RemoteDataSource
import com.infosys.telstra_assignment.data.repository.DataRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    private val http_client = OkHttpClient.Builder().addInterceptor(
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    ) // <-- this is the important line!

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl("https://dl.dropboxusercontent.com/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(http_client.build())
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideappService(retrofit: Retrofit): AppService = retrofit.create(AppService::class.java)

    @Singleton
    @Provides
    fun provideRemoteDataSource(appService: AppService) = RemoteDataSource(appService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun providedataDao(db: AppDatabase) = db.dataDao()

    @Singleton
    @Provides
    fun provideRepository(
        remoteDataSource: RemoteDataSource,
        localDataSource: DataDao
    ) =
        DataRepository(remoteDataSource, localDataSource)
}