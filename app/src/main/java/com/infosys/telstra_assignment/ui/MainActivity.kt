package com.infosys.telstra_assignment.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.infosys.telstra_assignment.databinding.MainActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: MainActivityBinding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}
