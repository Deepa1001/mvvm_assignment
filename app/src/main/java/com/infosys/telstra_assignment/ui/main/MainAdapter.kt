package com.infosys.telstra_assignment.ui.main

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.infosys.telstra_assignment.R
import com.infosys.telstra_assignment.data.entities.DataRowModel
import com.infosys.telstra_assignment.databinding.ItemRecyclerViewBinding

class MainAdapter(private val listener: ItemListener) : RecyclerView.Adapter<MainViewHolder>() {

    interface ItemListener {
        fun onClickedList(id: Int)
    }

    val items = ArrayList<DataRowModel>()

    fun setItems(items: List<DataRowModel>?) {
        this.items.clear()
        items?.let { this.items.addAll(it) }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val binding: ItemRecyclerViewBinding =
            ItemRecyclerViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MainViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) =
        holder.bind(items[position])
}

class MainViewHolder(
    private val itemBinding: ItemRecyclerViewBinding,
    private val listener: MainAdapter.ItemListener
) : RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener {

    lateinit var data_details_Model: DataRowModel

    init {
        itemBinding.root.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    fun bind(item: DataRowModel) {
        this.data_details_Model = item
        itemBinding.name.text = item.title
        itemBinding.details.text = item.description
        Glide.with(itemBinding.root)
            .load(item.imageHref)
            .apply(
                RequestOptions()
                    .transform(CenterCrop(), RoundedCorners(80))
            )
            .placeholder(R.mipmap.ic_launcher)
            .into(itemBinding.image)
    }

    override fun onClick(v: View?) {
//        listener.onClickedList(data_details_Model.title)
    }
}

