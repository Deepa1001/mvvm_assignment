package com.infosys.telstra_assignment.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.infosys.telstra_assignment.data.repository.DataRepository

class MainViewModel @ViewModelInject constructor(
     var repository: DataRepository
) : ViewModel() {

    val data_ = repository.getData()
}
